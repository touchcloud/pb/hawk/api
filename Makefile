.PHONY: v1 system

GRPC_VERSION := v1.39.0
GOPATH := $$HOME/go
PB_VERSION := v3.13.0
PROTO_VERSION := v1.27.1
PKG_PATH := gitlab.com/touchcloud/pb/hawk/api

dependency:
	@mkdir /tmp/protoc && \
	wget https://github.com/protocolbuffers/protobuf/releases/download/${PB_VERSION}/protoc-${PB_VERSION}-linux-x86_64.zip && \
	unzip protoc-${PB_VERSION}-linux-x86_64.zip -d /tmp/protoc && \
	sudo cp -rf /tmp/protoc/include/* /usr/local/include/ && \
	sudo cp -rf /tmp/protoc/bin/* /usr/local/bin && \
	go get github.com/grpc/grpc && \
	go get github.com/protocolbuffers/protobuf@${PB_VERSION} && \
	go get google.golang.org/protobuf/proto@${PROTO_VERSION} && \
	go get google.golang.org/protobuf/cmd/protoc-gen-go@${PROTO_VERSION}

v1: v1/*.proto
	@protoc --proto_path=${GOPATH}/pkg/mod/github.com/grpc/grpc@${GRPC_VERSION}/src \
	--proto_path=${GOPATH}/pkg/mod/github.com/protocolbuffers/protobuf@${PB_VERSION}+incompatible/src \
	--proto_path=. \
	--go_out=. v1/*.proto && \
	cp ${PKG_PATH}/v1/* v1/ && \
	rm -rf gitlab.com

system: system/*.proto
	@protoc --proto_path=${GOPATH}/pkg/mod/github.com/grpc/grpc@${GRPC_VERSION}/src \
	--proto_path=${GOPATH}/pkg/mod/github.com/protocolbuffers/protobuf@${PB_VERSION}+incompatible/src \
	--proto_path=. \
	--go_out=. system/*.proto && \
	cp ${PKG_PATH}/system/* system/ && \
	rm -rf gitlab.com

middleware: *.proto
	@protoc --proto_path=${GOPATH}/pkg/mod/github.com/grpc/grpc@${GRPC_VERSION}/src \
	--proto_path=${GOPATH}/pkg/mod/github.com/protocolbuffers/protobuf@${PB_VERSION}+incompatible/src \
	--proto_path=. \
	--go_out=. *.proto && \
	cp ${PKG_PATH}/* . && \
	rm -rf gitlab.com
