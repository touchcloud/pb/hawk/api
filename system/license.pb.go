// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.25.0
// 	protoc        v3.13.0
// source: system/license.proto

package system

import (
	proto "github.com/golang/protobuf/proto"
	pb "gitlab.com/touchcloud/key/pb"
	status "google.golang.org/genproto/googleapis/rpc/status"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

// Type:    RESTful
// Method:  GET
// URL:     /system/support/license/kcode
//          /system/support/license/pcode
type SupportLicenseCodeResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Types  map[int32]string `protobuf:"bytes,1,rep,name=types,proto3" json:"types,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	Status *status.Status   `protobuf:"bytes,2,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *SupportLicenseCodeResponse) Reset() {
	*x = SupportLicenseCodeResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_system_license_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SupportLicenseCodeResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SupportLicenseCodeResponse) ProtoMessage() {}

func (x *SupportLicenseCodeResponse) ProtoReflect() protoreflect.Message {
	mi := &file_system_license_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SupportLicenseCodeResponse.ProtoReflect.Descriptor instead.
func (*SupportLicenseCodeResponse) Descriptor() ([]byte, []int) {
	return file_system_license_proto_rawDescGZIP(), []int{0}
}

func (x *SupportLicenseCodeResponse) GetTypes() map[int32]string {
	if x != nil {
		return x.Types
	}
	return nil
}

func (x *SupportLicenseCodeResponse) GetStatus() *status.Status {
	if x != nil {
		return x.Status
	}
	return nil
}

// Type:    RESTful
// Method:  POST
// URL:     /system/license
type RegisterLicenseRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Key string `protobuf:"bytes,1,opt,name=key,proto3" json:"key,omitempty"`
}

func (x *RegisterLicenseRequest) Reset() {
	*x = RegisterLicenseRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_system_license_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *RegisterLicenseRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*RegisterLicenseRequest) ProtoMessage() {}

func (x *RegisterLicenseRequest) ProtoReflect() protoreflect.Message {
	mi := &file_system_license_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use RegisterLicenseRequest.ProtoReflect.Descriptor instead.
func (*RegisterLicenseRequest) Descriptor() ([]byte, []int) {
	return file_system_license_proto_rawDescGZIP(), []int{1}
}

func (x *RegisterLicenseRequest) GetKey() string {
	if x != nil {
		return x.Key
	}
	return ""
}

type RegisterLicenseResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Error   string         `protobuf:"bytes,1,opt,name=error,proto3" json:"error,omitempty"`
	License *pb.KeyData    `protobuf:"bytes,2,opt,name=license,proto3" json:"license,omitempty"`
	Status  *status.Status `protobuf:"bytes,3,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *RegisterLicenseResponse) Reset() {
	*x = RegisterLicenseResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_system_license_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *RegisterLicenseResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*RegisterLicenseResponse) ProtoMessage() {}

func (x *RegisterLicenseResponse) ProtoReflect() protoreflect.Message {
	mi := &file_system_license_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use RegisterLicenseResponse.ProtoReflect.Descriptor instead.
func (*RegisterLicenseResponse) Descriptor() ([]byte, []int) {
	return file_system_license_proto_rawDescGZIP(), []int{2}
}

func (x *RegisterLicenseResponse) GetError() string {
	if x != nil {
		return x.Error
	}
	return ""
}

func (x *RegisterLicenseResponse) GetLicense() *pb.KeyData {
	if x != nil {
		return x.License
	}
	return nil
}

func (x *RegisterLicenseResponse) GetStatus() *status.Status {
	if x != nil {
		return x.Status
	}
	return nil
}

// Type:    RESTful
// Method:  GET
// URL:     /system/license
type ReadLicenseResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Key     string         `protobuf:"bytes,1,opt,name=key,proto3" json:"key,omitempty"`
	Error   string         `protobuf:"bytes,2,opt,name=error,proto3" json:"error,omitempty"`
	License *pb.KeyData    `protobuf:"bytes,3,opt,name=license,proto3" json:"license,omitempty"`
	Status  *status.Status `protobuf:"bytes,4,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *ReadLicenseResponse) Reset() {
	*x = ReadLicenseResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_system_license_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ReadLicenseResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ReadLicenseResponse) ProtoMessage() {}

func (x *ReadLicenseResponse) ProtoReflect() protoreflect.Message {
	mi := &file_system_license_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ReadLicenseResponse.ProtoReflect.Descriptor instead.
func (*ReadLicenseResponse) Descriptor() ([]byte, []int) {
	return file_system_license_proto_rawDescGZIP(), []int{3}
}

func (x *ReadLicenseResponse) GetKey() string {
	if x != nil {
		return x.Key
	}
	return ""
}

func (x *ReadLicenseResponse) GetError() string {
	if x != nil {
		return x.Error
	}
	return ""
}

func (x *ReadLicenseResponse) GetLicense() *pb.KeyData {
	if x != nil {
		return x.License
	}
	return nil
}

func (x *ReadLicenseResponse) GetStatus() *status.Status {
	if x != nil {
		return x.Status
	}
	return nil
}

var File_system_license_proto protoreflect.FileDescriptor

var file_system_license_proto_rawDesc = []byte{
	0x0a, 0x14, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x2f, 0x6c, 0x69, 0x63, 0x65, 0x6e, 0x73, 0x65,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x1d, 0x74, 0x6f, 0x75, 0x63, 0x68, 0x63, 0x6c, 0x6f,
	0x75, 0x64, 0x2e, 0x70, 0x62, 0x2e, 0x68, 0x61, 0x77, 0x6b, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x73,
	0x79, 0x73, 0x74, 0x65, 0x6d, 0x1a, 0x1e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x67, 0x72, 0x70,
	0x63, 0x2f, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x2f, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x09, 0x6b, 0x65, 0x79, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x22, 0xde, 0x01, 0x0a, 0x1a, 0x53, 0x75, 0x70, 0x70, 0x6f, 0x72, 0x74, 0x4c, 0x69, 0x63, 0x65,
	0x6e, 0x73, 0x65, 0x43, 0x6f, 0x64, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12,
	0x5a, 0x0a, 0x05, 0x74, 0x79, 0x70, 0x65, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x44,
	0x2e, 0x74, 0x6f, 0x75, 0x63, 0x68, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x70, 0x62, 0x2e, 0x68,
	0x61, 0x77, 0x6b, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x2e, 0x53,
	0x75, 0x70, 0x70, 0x6f, 0x72, 0x74, 0x4c, 0x69, 0x63, 0x65, 0x6e, 0x73, 0x65, 0x43, 0x6f, 0x64,
	0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x2e, 0x54, 0x79, 0x70, 0x65, 0x73, 0x45,
	0x6e, 0x74, 0x72, 0x79, 0x52, 0x05, 0x74, 0x79, 0x70, 0x65, 0x73, 0x12, 0x2a, 0x0a, 0x06, 0x73,
	0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x12, 0x2e, 0x67, 0x6f,
	0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x72, 0x70, 0x63, 0x2e, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x52,
	0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x1a, 0x38, 0x0a, 0x0a, 0x54, 0x79, 0x70, 0x65, 0x73,
	0x45, 0x6e, 0x74, 0x72, 0x79, 0x12, 0x10, 0x0a, 0x03, 0x6b, 0x65, 0x79, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x05, 0x52, 0x03, 0x6b, 0x65, 0x79, 0x12, 0x14, 0x0a, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x3a, 0x02, 0x38,
	0x01, 0x22, 0x2a, 0x0a, 0x16, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x65, 0x72, 0x4c, 0x69, 0x63,
	0x65, 0x6e, 0x73, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x10, 0x0a, 0x03, 0x6b,
	0x65, 0x79, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x6b, 0x65, 0x79, 0x22, 0x8e, 0x01,
	0x0a, 0x17, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x65, 0x72, 0x4c, 0x69, 0x63, 0x65, 0x6e, 0x73,
	0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x65, 0x72, 0x72,
	0x6f, 0x72, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x65, 0x72, 0x72, 0x6f, 0x72, 0x12,
	0x31, 0x0a, 0x07, 0x6c, 0x69, 0x63, 0x65, 0x6e, 0x73, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b,
	0x32, 0x17, 0x2e, 0x74, 0x6f, 0x75, 0x63, 0x68, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x6b, 0x65,
	0x79, 0x2e, 0x4b, 0x65, 0x79, 0x44, 0x61, 0x74, 0x61, 0x52, 0x07, 0x6c, 0x69, 0x63, 0x65, 0x6e,
	0x73, 0x65, 0x12, 0x2a, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x03, 0x20, 0x01,
	0x28, 0x0b, 0x32, 0x12, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x72, 0x70, 0x63, 0x2e,
	0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x22, 0x9c,
	0x01, 0x0a, 0x13, 0x52, 0x65, 0x61, 0x64, 0x4c, 0x69, 0x63, 0x65, 0x6e, 0x73, 0x65, 0x52, 0x65,
	0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x10, 0x0a, 0x03, 0x6b, 0x65, 0x79, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x03, 0x6b, 0x65, 0x79, 0x12, 0x14, 0x0a, 0x05, 0x65, 0x72, 0x72, 0x6f,
	0x72, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x65, 0x72, 0x72, 0x6f, 0x72, 0x12, 0x31,
	0x0a, 0x07, 0x6c, 0x69, 0x63, 0x65, 0x6e, 0x73, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0b, 0x32,
	0x17, 0x2e, 0x74, 0x6f, 0x75, 0x63, 0x68, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x6b, 0x65, 0x79,
	0x2e, 0x4b, 0x65, 0x79, 0x44, 0x61, 0x74, 0x61, 0x52, 0x07, 0x6c, 0x69, 0x63, 0x65, 0x6e, 0x73,
	0x65, 0x12, 0x2a, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x04, 0x20, 0x01, 0x28,
	0x0b, 0x32, 0x12, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x72, 0x70, 0x63, 0x2e, 0x53,
	0x74, 0x61, 0x74, 0x75, 0x73, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x42, 0x31, 0x5a,
	0x2f, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x74, 0x6f, 0x75, 0x63,
	0x68, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2f, 0x70, 0x62, 0x2f, 0x68, 0x61, 0x77, 0x6b, 0x2f, 0x61,
	0x70, 0x69, 0x2f, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x3b, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6d,
	0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_system_license_proto_rawDescOnce sync.Once
	file_system_license_proto_rawDescData = file_system_license_proto_rawDesc
)

func file_system_license_proto_rawDescGZIP() []byte {
	file_system_license_proto_rawDescOnce.Do(func() {
		file_system_license_proto_rawDescData = protoimpl.X.CompressGZIP(file_system_license_proto_rawDescData)
	})
	return file_system_license_proto_rawDescData
}

var file_system_license_proto_msgTypes = make([]protoimpl.MessageInfo, 5)
var file_system_license_proto_goTypes = []interface{}{
	(*SupportLicenseCodeResponse)(nil), // 0: touchcloud.pb.hawk.api.system.SupportLicenseCodeResponse
	(*RegisterLicenseRequest)(nil),     // 1: touchcloud.pb.hawk.api.system.RegisterLicenseRequest
	(*RegisterLicenseResponse)(nil),    // 2: touchcloud.pb.hawk.api.system.RegisterLicenseResponse
	(*ReadLicenseResponse)(nil),        // 3: touchcloud.pb.hawk.api.system.ReadLicenseResponse
	nil,                                // 4: touchcloud.pb.hawk.api.system.SupportLicenseCodeResponse.TypesEntry
	(*status.Status)(nil),              // 5: google.rpc.Status
	(*pb.KeyData)(nil),                 // 6: touchcloud.key.KeyData
}
var file_system_license_proto_depIdxs = []int32{
	4, // 0: touchcloud.pb.hawk.api.system.SupportLicenseCodeResponse.types:type_name -> touchcloud.pb.hawk.api.system.SupportLicenseCodeResponse.TypesEntry
	5, // 1: touchcloud.pb.hawk.api.system.SupportLicenseCodeResponse.status:type_name -> google.rpc.Status
	6, // 2: touchcloud.pb.hawk.api.system.RegisterLicenseResponse.license:type_name -> touchcloud.key.KeyData
	5, // 3: touchcloud.pb.hawk.api.system.RegisterLicenseResponse.status:type_name -> google.rpc.Status
	6, // 4: touchcloud.pb.hawk.api.system.ReadLicenseResponse.license:type_name -> touchcloud.key.KeyData
	5, // 5: touchcloud.pb.hawk.api.system.ReadLicenseResponse.status:type_name -> google.rpc.Status
	6, // [6:6] is the sub-list for method output_type
	6, // [6:6] is the sub-list for method input_type
	6, // [6:6] is the sub-list for extension type_name
	6, // [6:6] is the sub-list for extension extendee
	0, // [0:6] is the sub-list for field type_name
}

func init() { file_system_license_proto_init() }
func file_system_license_proto_init() {
	if File_system_license_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_system_license_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SupportLicenseCodeResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_system_license_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*RegisterLicenseRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_system_license_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*RegisterLicenseResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_system_license_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ReadLicenseResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_system_license_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   5,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_system_license_proto_goTypes,
		DependencyIndexes: file_system_license_proto_depIdxs,
		MessageInfos:      file_system_license_proto_msgTypes,
	}.Build()
	File_system_license_proto = out.File
	file_system_license_proto_rawDesc = nil
	file_system_license_proto_goTypes = nil
	file_system_license_proto_depIdxs = nil
}
