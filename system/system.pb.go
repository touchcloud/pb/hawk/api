// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.25.0
// 	protoc        v3.13.0
// source: system/system.proto

package system

import (
	proto "github.com/golang/protobuf/proto"
	status "google.golang.org/genproto/googleapis/rpc/status"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

//
// Type: 	RESTful
// Method: 	POST
// URL: 	/system/login
//
type LoginRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	User              *UserInfo      `protobuf:"bytes,1,opt,name=user,proto3" json:"user,omitempty"`
	LogoutOtherDevice bool           `protobuf:"varint,2,opt,name=logout_other_device,json=logoutOtherDevice,proto3" json:"logout_other_device,omitempty"`
	Status            *status.Status `protobuf:"bytes,3,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *LoginRequest) Reset() {
	*x = LoginRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_system_system_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *LoginRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*LoginRequest) ProtoMessage() {}

func (x *LoginRequest) ProtoReflect() protoreflect.Message {
	mi := &file_system_system_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use LoginRequest.ProtoReflect.Descriptor instead.
func (*LoginRequest) Descriptor() ([]byte, []int) {
	return file_system_system_proto_rawDescGZIP(), []int{0}
}

func (x *LoginRequest) GetUser() *UserInfo {
	if x != nil {
		return x.User
	}
	return nil
}

func (x *LoginRequest) GetLogoutOtherDevice() bool {
	if x != nil {
		return x.LogoutOtherDevice
	}
	return false
}

func (x *LoginRequest) GetStatus() *status.Status {
	if x != nil {
		return x.Status
	}
	return nil
}

type LoginResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Token  string         `protobuf:"bytes,1,opt,name=token,proto3" json:"token,omitempty"`
	Status *status.Status `protobuf:"bytes,2,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *LoginResponse) Reset() {
	*x = LoginResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_system_system_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *LoginResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*LoginResponse) ProtoMessage() {}

func (x *LoginResponse) ProtoReflect() protoreflect.Message {
	mi := &file_system_system_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use LoginResponse.ProtoReflect.Descriptor instead.
func (*LoginResponse) Descriptor() ([]byte, []int) {
	return file_system_system_proto_rawDescGZIP(), []int{1}
}

func (x *LoginResponse) GetToken() string {
	if x != nil {
		return x.Token
	}
	return ""
}

func (x *LoginResponse) GetStatus() *status.Status {
	if x != nil {
		return x.Status
	}
	return nil
}

//
// Type: 	RESTful
// Method: 	POST
// URL: 	/system/logout
//
type LogoutReponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Status *status.Status `protobuf:"bytes,2,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *LogoutReponse) Reset() {
	*x = LogoutReponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_system_system_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *LogoutReponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*LogoutReponse) ProtoMessage() {}

func (x *LogoutReponse) ProtoReflect() protoreflect.Message {
	mi := &file_system_system_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use LogoutReponse.ProtoReflect.Descriptor instead.
func (*LogoutReponse) Descriptor() ([]byte, []int) {
	return file_system_system_proto_rawDescGZIP(), []int{2}
}

func (x *LogoutReponse) GetStatus() *status.Status {
	if x != nil {
		return x.Status
	}
	return nil
}

//
// Type: 	RESTful
// Method: 	PUT
// URL: 	/system/refresh
//
type RefreshTokenResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Token  string         `protobuf:"bytes,1,opt,name=token,proto3" json:"token,omitempty"`
	Status *status.Status `protobuf:"bytes,2,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *RefreshTokenResponse) Reset() {
	*x = RefreshTokenResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_system_system_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *RefreshTokenResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*RefreshTokenResponse) ProtoMessage() {}

func (x *RefreshTokenResponse) ProtoReflect() protoreflect.Message {
	mi := &file_system_system_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use RefreshTokenResponse.ProtoReflect.Descriptor instead.
func (*RefreshTokenResponse) Descriptor() ([]byte, []int) {
	return file_system_system_proto_rawDescGZIP(), []int{3}
}

func (x *RefreshTokenResponse) GetToken() string {
	if x != nil {
		return x.Token
	}
	return ""
}

func (x *RefreshTokenResponse) GetStatus() *status.Status {
	if x != nil {
		return x.Status
	}
	return nil
}

var File_system_system_proto protoreflect.FileDescriptor

var file_system_system_proto_rawDesc = []byte{
	0x0a, 0x13, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x2f, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x1d, 0x74, 0x6f, 0x75, 0x63, 0x68, 0x63, 0x6c, 0x6f, 0x75,
	0x64, 0x2e, 0x70, 0x62, 0x2e, 0x68, 0x61, 0x77, 0x6b, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x73, 0x79,
	0x73, 0x74, 0x65, 0x6d, 0x1a, 0x1e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x67, 0x72, 0x70, 0x63,
	0x2f, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x2f, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x11, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x2f, 0x75, 0x73, 0x65,
	0x72, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0xa7, 0x01, 0x0a, 0x0c, 0x4c, 0x6f, 0x67, 0x69,
	0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x3b, 0x0a, 0x04, 0x75, 0x73, 0x65, 0x72,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x27, 0x2e, 0x74, 0x6f, 0x75, 0x63, 0x68, 0x63, 0x6c,
	0x6f, 0x75, 0x64, 0x2e, 0x70, 0x62, 0x2e, 0x68, 0x61, 0x77, 0x6b, 0x2e, 0x61, 0x70, 0x69, 0x2e,
	0x73, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x2e, 0x55, 0x73, 0x65, 0x72, 0x49, 0x6e, 0x66, 0x6f, 0x52,
	0x04, 0x75, 0x73, 0x65, 0x72, 0x12, 0x2e, 0x0a, 0x13, 0x6c, 0x6f, 0x67, 0x6f, 0x75, 0x74, 0x5f,
	0x6f, 0x74, 0x68, 0x65, 0x72, 0x5f, 0x64, 0x65, 0x76, 0x69, 0x63, 0x65, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x08, 0x52, 0x11, 0x6c, 0x6f, 0x67, 0x6f, 0x75, 0x74, 0x4f, 0x74, 0x68, 0x65, 0x72, 0x44,
	0x65, 0x76, 0x69, 0x63, 0x65, 0x12, 0x2a, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18,
	0x03, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x12, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x72,
	0x70, 0x63, 0x2e, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75,
	0x73, 0x22, 0x51, 0x0a, 0x0d, 0x4c, 0x6f, 0x67, 0x69, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e,
	0x73, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x74, 0x6f, 0x6b, 0x65, 0x6e, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x05, 0x74, 0x6f, 0x6b, 0x65, 0x6e, 0x12, 0x2a, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74,
	0x75, 0x73, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x12, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c,
	0x65, 0x2e, 0x72, 0x70, 0x63, 0x2e, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x52, 0x06, 0x73, 0x74,
	0x61, 0x74, 0x75, 0x73, 0x22, 0x3b, 0x0a, 0x0d, 0x4c, 0x6f, 0x67, 0x6f, 0x75, 0x74, 0x52, 0x65,
	0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x2a, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x12, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x72,
	0x70, 0x63, 0x2e, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75,
	0x73, 0x22, 0x58, 0x0a, 0x14, 0x52, 0x65, 0x66, 0x72, 0x65, 0x73, 0x68, 0x54, 0x6f, 0x6b, 0x65,
	0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x74, 0x6f, 0x6b,
	0x65, 0x6e, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x74, 0x6f, 0x6b, 0x65, 0x6e, 0x12,
	0x2a, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32,
	0x12, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x72, 0x70, 0x63, 0x2e, 0x53, 0x74, 0x61,
	0x74, 0x75, 0x73, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x42, 0x31, 0x5a, 0x2f, 0x67,
	0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x74, 0x6f, 0x75, 0x63, 0x68, 0x63,
	0x6c, 0x6f, 0x75, 0x64, 0x2f, 0x70, 0x62, 0x2f, 0x68, 0x61, 0x77, 0x6b, 0x2f, 0x61, 0x70, 0x69,
	0x2f, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x3b, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x62, 0x06,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_system_system_proto_rawDescOnce sync.Once
	file_system_system_proto_rawDescData = file_system_system_proto_rawDesc
)

func file_system_system_proto_rawDescGZIP() []byte {
	file_system_system_proto_rawDescOnce.Do(func() {
		file_system_system_proto_rawDescData = protoimpl.X.CompressGZIP(file_system_system_proto_rawDescData)
	})
	return file_system_system_proto_rawDescData
}

var file_system_system_proto_msgTypes = make([]protoimpl.MessageInfo, 4)
var file_system_system_proto_goTypes = []interface{}{
	(*LoginRequest)(nil),         // 0: touchcloud.pb.hawk.api.system.LoginRequest
	(*LoginResponse)(nil),        // 1: touchcloud.pb.hawk.api.system.LoginResponse
	(*LogoutReponse)(nil),        // 2: touchcloud.pb.hawk.api.system.LogoutReponse
	(*RefreshTokenResponse)(nil), // 3: touchcloud.pb.hawk.api.system.RefreshTokenResponse
	(*UserInfo)(nil),             // 4: touchcloud.pb.hawk.api.system.UserInfo
	(*status.Status)(nil),        // 5: google.rpc.Status
}
var file_system_system_proto_depIdxs = []int32{
	4, // 0: touchcloud.pb.hawk.api.system.LoginRequest.user:type_name -> touchcloud.pb.hawk.api.system.UserInfo
	5, // 1: touchcloud.pb.hawk.api.system.LoginRequest.status:type_name -> google.rpc.Status
	5, // 2: touchcloud.pb.hawk.api.system.LoginResponse.status:type_name -> google.rpc.Status
	5, // 3: touchcloud.pb.hawk.api.system.LogoutReponse.status:type_name -> google.rpc.Status
	5, // 4: touchcloud.pb.hawk.api.system.RefreshTokenResponse.status:type_name -> google.rpc.Status
	5, // [5:5] is the sub-list for method output_type
	5, // [5:5] is the sub-list for method input_type
	5, // [5:5] is the sub-list for extension type_name
	5, // [5:5] is the sub-list for extension extendee
	0, // [0:5] is the sub-list for field type_name
}

func init() { file_system_system_proto_init() }
func file_system_system_proto_init() {
	if File_system_system_proto != nil {
		return
	}
	file_system_user_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_system_system_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*LoginRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_system_system_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*LoginResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_system_system_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*LogoutReponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_system_system_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*RefreshTokenResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_system_system_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   4,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_system_system_proto_goTypes,
		DependencyIndexes: file_system_system_proto_depIdxs,
		MessageInfos:      file_system_system_proto_msgTypes,
	}.Build()
	File_system_system_proto = out.File
	file_system_system_proto_rawDesc = nil
	file_system_system_proto_goTypes = nil
	file_system_system_proto_depIdxs = nil
}
