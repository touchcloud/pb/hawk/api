// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.25.0
// 	protoc        v3.13.0
// source: v1/channel.proto

package v1

import (
	proto "github.com/golang/protobuf/proto"
	status "google.golang.org/genproto/googleapis/rpc/status"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type Channel struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Uuid        string            `protobuf:"bytes,1,opt,name=uuid,proto3" json:"uuid,omitempty"`
	Type        string            `protobuf:"bytes,2,opt,name=type,proto3" json:"type,omitempty"`
	Name        string            `protobuf:"bytes,3,opt,name=name,proto3" json:"name,omitempty"`
	FeederId    string            `protobuf:"bytes,4,opt,name=feeder_id,json=feederId,proto3" json:"feeder_id,omitempty"`
	EngineId    string            `protobuf:"bytes,5,opt,name=engine_id,json=engineId,proto3" json:"engine_id,omitempty"`
	LabelFilter map[uint32]string `protobuf:"bytes,6,rep,name=label_filter,json=labelFilter,proto3" json:"label_filter,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	Confidence  float32           `protobuf:"fixed32,7,opt,name=confidence,proto3" json:"confidence,omitempty"`
	Fps         float32           `protobuf:"fixed32,8,opt,name=fps,proto3" json:"fps,omitempty"`
}

func (x *Channel) Reset() {
	*x = Channel{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_channel_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Channel) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Channel) ProtoMessage() {}

func (x *Channel) ProtoReflect() protoreflect.Message {
	mi := &file_v1_channel_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Channel.ProtoReflect.Descriptor instead.
func (*Channel) Descriptor() ([]byte, []int) {
	return file_v1_channel_proto_rawDescGZIP(), []int{0}
}

func (x *Channel) GetUuid() string {
	if x != nil {
		return x.Uuid
	}
	return ""
}

func (x *Channel) GetType() string {
	if x != nil {
		return x.Type
	}
	return ""
}

func (x *Channel) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *Channel) GetFeederId() string {
	if x != nil {
		return x.FeederId
	}
	return ""
}

func (x *Channel) GetEngineId() string {
	if x != nil {
		return x.EngineId
	}
	return ""
}

func (x *Channel) GetLabelFilter() map[uint32]string {
	if x != nil {
		return x.LabelFilter
	}
	return nil
}

func (x *Channel) GetConfidence() float32 {
	if x != nil {
		return x.Confidence
	}
	return 0
}

func (x *Channel) GetFps() float32 {
	if x != nil {
		return x.Fps
	}
	return 0
}

// Method:  GET
// URL:     /v1/support/type/channel
type SupportChannelTypeResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Types  []string       `protobuf:"bytes,1,rep,name=types,proto3" json:"types,omitempty"`
	Status *status.Status `protobuf:"bytes,2,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *SupportChannelTypeResponse) Reset() {
	*x = SupportChannelTypeResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_channel_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SupportChannelTypeResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SupportChannelTypeResponse) ProtoMessage() {}

func (x *SupportChannelTypeResponse) ProtoReflect() protoreflect.Message {
	mi := &file_v1_channel_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SupportChannelTypeResponse.ProtoReflect.Descriptor instead.
func (*SupportChannelTypeResponse) Descriptor() ([]byte, []int) {
	return file_v1_channel_proto_rawDescGZIP(), []int{1}
}

func (x *SupportChannelTypeResponse) GetTypes() []string {
	if x != nil {
		return x.Types
	}
	return nil
}

func (x *SupportChannelTypeResponse) GetStatus() *status.Status {
	if x != nil {
		return x.Status
	}
	return nil
}

// Method:  POST
// URL:     /v1/channels
type CreateChannelRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Type        string            `protobuf:"bytes,1,opt,name=type,proto3" json:"type,omitempty"`
	Name        string            `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	LabelFilter map[uint32]string `protobuf:"bytes,3,rep,name=label_filter,json=labelFilter,proto3" json:"label_filter,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	Confidence  float32           `protobuf:"fixed32,4,opt,name=confidence,proto3" json:"confidence,omitempty"`
}

func (x *CreateChannelRequest) Reset() {
	*x = CreateChannelRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_channel_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateChannelRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateChannelRequest) ProtoMessage() {}

func (x *CreateChannelRequest) ProtoReflect() protoreflect.Message {
	mi := &file_v1_channel_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateChannelRequest.ProtoReflect.Descriptor instead.
func (*CreateChannelRequest) Descriptor() ([]byte, []int) {
	return file_v1_channel_proto_rawDescGZIP(), []int{2}
}

func (x *CreateChannelRequest) GetType() string {
	if x != nil {
		return x.Type
	}
	return ""
}

func (x *CreateChannelRequest) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *CreateChannelRequest) GetLabelFilter() map[uint32]string {
	if x != nil {
		return x.LabelFilter
	}
	return nil
}

func (x *CreateChannelRequest) GetConfidence() float32 {
	if x != nil {
		return x.Confidence
	}
	return 0
}

type CreateChannelResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Channel *Channel       `protobuf:"bytes,1,opt,name=channel,proto3" json:"channel,omitempty"`
	Status  *status.Status `protobuf:"bytes,2,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *CreateChannelResponse) Reset() {
	*x = CreateChannelResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_channel_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateChannelResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateChannelResponse) ProtoMessage() {}

func (x *CreateChannelResponse) ProtoReflect() protoreflect.Message {
	mi := &file_v1_channel_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateChannelResponse.ProtoReflect.Descriptor instead.
func (*CreateChannelResponse) Descriptor() ([]byte, []int) {
	return file_v1_channel_proto_rawDescGZIP(), []int{3}
}

func (x *CreateChannelResponse) GetChannel() *Channel {
	if x != nil {
		return x.Channel
	}
	return nil
}

func (x *CreateChannelResponse) GetStatus() *status.Status {
	if x != nil {
		return x.Status
	}
	return nil
}

// Method:  GET
// URL:     /v1/channels
type ListChannelResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Channels []*Channel     `protobuf:"bytes,1,rep,name=channels,proto3" json:"channels,omitempty"`
	Status   *status.Status `protobuf:"bytes,2,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *ListChannelResponse) Reset() {
	*x = ListChannelResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_channel_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ListChannelResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ListChannelResponse) ProtoMessage() {}

func (x *ListChannelResponse) ProtoReflect() protoreflect.Message {
	mi := &file_v1_channel_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ListChannelResponse.ProtoReflect.Descriptor instead.
func (*ListChannelResponse) Descriptor() ([]byte, []int) {
	return file_v1_channel_proto_rawDescGZIP(), []int{4}
}

func (x *ListChannelResponse) GetChannels() []*Channel {
	if x != nil {
		return x.Channels
	}
	return nil
}

func (x *ListChannelResponse) GetStatus() *status.Status {
	if x != nil {
		return x.Status
	}
	return nil
}

// Method:  PUT
// URL:     /v1/channels/:uuid
type UpdateChannelRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Name        string            `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	LabelFilter map[uint32]string `protobuf:"bytes,2,rep,name=label_filter,json=labelFilter,proto3" json:"label_filter,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	Confidence  float32           `protobuf:"fixed32,3,opt,name=confidence,proto3" json:"confidence,omitempty"`
}

func (x *UpdateChannelRequest) Reset() {
	*x = UpdateChannelRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_channel_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateChannelRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateChannelRequest) ProtoMessage() {}

func (x *UpdateChannelRequest) ProtoReflect() protoreflect.Message {
	mi := &file_v1_channel_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateChannelRequest.ProtoReflect.Descriptor instead.
func (*UpdateChannelRequest) Descriptor() ([]byte, []int) {
	return file_v1_channel_proto_rawDescGZIP(), []int{5}
}

func (x *UpdateChannelRequest) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *UpdateChannelRequest) GetLabelFilter() map[uint32]string {
	if x != nil {
		return x.LabelFilter
	}
	return nil
}

func (x *UpdateChannelRequest) GetConfidence() float32 {
	if x != nil {
		return x.Confidence
	}
	return 0
}

type UpdateChannelResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Channel *Channel       `protobuf:"bytes,1,opt,name=channel,proto3" json:"channel,omitempty"`
	Status  *status.Status `protobuf:"bytes,2,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *UpdateChannelResponse) Reset() {
	*x = UpdateChannelResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_channel_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateChannelResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateChannelResponse) ProtoMessage() {}

func (x *UpdateChannelResponse) ProtoReflect() protoreflect.Message {
	mi := &file_v1_channel_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateChannelResponse.ProtoReflect.Descriptor instead.
func (*UpdateChannelResponse) Descriptor() ([]byte, []int) {
	return file_v1_channel_proto_rawDescGZIP(), []int{6}
}

func (x *UpdateChannelResponse) GetChannel() *Channel {
	if x != nil {
		return x.Channel
	}
	return nil
}

func (x *UpdateChannelResponse) GetStatus() *status.Status {
	if x != nil {
		return x.Status
	}
	return nil
}

// Method:  DELETE
// URL:     /v1/channels/:uuid
type DeleteChannelResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Channel *Channel       `protobuf:"bytes,1,opt,name=channel,proto3" json:"channel,omitempty"`
	Status  *status.Status `protobuf:"bytes,2,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *DeleteChannelResponse) Reset() {
	*x = DeleteChannelResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_channel_proto_msgTypes[7]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DeleteChannelResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DeleteChannelResponse) ProtoMessage() {}

func (x *DeleteChannelResponse) ProtoReflect() protoreflect.Message {
	mi := &file_v1_channel_proto_msgTypes[7]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DeleteChannelResponse.ProtoReflect.Descriptor instead.
func (*DeleteChannelResponse) Descriptor() ([]byte, []int) {
	return file_v1_channel_proto_rawDescGZIP(), []int{7}
}

func (x *DeleteChannelResponse) GetChannel() *Channel {
	if x != nil {
		return x.Channel
	}
	return nil
}

func (x *DeleteChannelResponse) GetStatus() *status.Status {
	if x != nil {
		return x.Status
	}
	return nil
}

// Method:  PUT
// URL:     /v1/channels/:uuid/feeder/:fuid/*action
// Action:  [mount, unmount]
type MountFeederResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Channel *Channel       `protobuf:"bytes,1,opt,name=channel,proto3" json:"channel,omitempty"`
	Status  *status.Status `protobuf:"bytes,2,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *MountFeederResponse) Reset() {
	*x = MountFeederResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_channel_proto_msgTypes[8]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *MountFeederResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*MountFeederResponse) ProtoMessage() {}

func (x *MountFeederResponse) ProtoReflect() protoreflect.Message {
	mi := &file_v1_channel_proto_msgTypes[8]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use MountFeederResponse.ProtoReflect.Descriptor instead.
func (*MountFeederResponse) Descriptor() ([]byte, []int) {
	return file_v1_channel_proto_rawDescGZIP(), []int{8}
}

func (x *MountFeederResponse) GetChannel() *Channel {
	if x != nil {
		return x.Channel
	}
	return nil
}

func (x *MountFeederResponse) GetStatus() *status.Status {
	if x != nil {
		return x.Status
	}
	return nil
}

// Method:  PUT
// URL:     /v1/channels/:uuid/engine/:euid/*action
// Action:  [mount, unmount]
type MountEngineResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Channel *Channel       `protobuf:"bytes,1,opt,name=channel,proto3" json:"channel,omitempty"`
	Status  *status.Status `protobuf:"bytes,2,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *MountEngineResponse) Reset() {
	*x = MountEngineResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_channel_proto_msgTypes[9]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *MountEngineResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*MountEngineResponse) ProtoMessage() {}

func (x *MountEngineResponse) ProtoReflect() protoreflect.Message {
	mi := &file_v1_channel_proto_msgTypes[9]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use MountEngineResponse.ProtoReflect.Descriptor instead.
func (*MountEngineResponse) Descriptor() ([]byte, []int) {
	return file_v1_channel_proto_rawDescGZIP(), []int{9}
}

func (x *MountEngineResponse) GetChannel() *Channel {
	if x != nil {
		return x.Channel
	}
	return nil
}

func (x *MountEngineResponse) GetStatus() *status.Status {
	if x != nil {
		return x.Status
	}
	return nil
}

var File_v1_channel_proto protoreflect.FileDescriptor

var file_v1_channel_proto_rawDesc = []byte{
	0x0a, 0x10, 0x76, 0x31, 0x2f, 0x63, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x12, 0x19, 0x74, 0x6f, 0x75, 0x63, 0x68, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x70,
	0x62, 0x2e, 0x68, 0x61, 0x77, 0x6b, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x76, 0x31, 0x1a, 0x1e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x67, 0x72, 0x70, 0x63, 0x2f, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73,
	0x2f, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0xc9, 0x02,
	0x0a, 0x07, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x12, 0x12, 0x0a, 0x04, 0x75, 0x75, 0x69,
	0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x75, 0x75, 0x69, 0x64, 0x12, 0x12, 0x0a,
	0x04, 0x74, 0x79, 0x70, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x74, 0x79, 0x70,
	0x65, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x1b, 0x0a, 0x09, 0x66, 0x65, 0x65, 0x64, 0x65, 0x72, 0x5f,
	0x69, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x66, 0x65, 0x65, 0x64, 0x65, 0x72,
	0x49, 0x64, 0x12, 0x1b, 0x0a, 0x09, 0x65, 0x6e, 0x67, 0x69, 0x6e, 0x65, 0x5f, 0x69, 0x64, 0x18,
	0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x65, 0x6e, 0x67, 0x69, 0x6e, 0x65, 0x49, 0x64, 0x12,
	0x56, 0x0a, 0x0c, 0x6c, 0x61, 0x62, 0x65, 0x6c, 0x5f, 0x66, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x18,
	0x06, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x33, 0x2e, 0x74, 0x6f, 0x75, 0x63, 0x68, 0x63, 0x6c, 0x6f,
	0x75, 0x64, 0x2e, 0x70, 0x62, 0x2e, 0x68, 0x61, 0x77, 0x6b, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x76,
	0x31, 0x2e, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x2e, 0x4c, 0x61, 0x62, 0x65, 0x6c, 0x46,
	0x69, 0x6c, 0x74, 0x65, 0x72, 0x45, 0x6e, 0x74, 0x72, 0x79, 0x52, 0x0b, 0x6c, 0x61, 0x62, 0x65,
	0x6c, 0x46, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x12, 0x1e, 0x0a, 0x0a, 0x63, 0x6f, 0x6e, 0x66, 0x69,
	0x64, 0x65, 0x6e, 0x63, 0x65, 0x18, 0x07, 0x20, 0x01, 0x28, 0x02, 0x52, 0x0a, 0x63, 0x6f, 0x6e,
	0x66, 0x69, 0x64, 0x65, 0x6e, 0x63, 0x65, 0x12, 0x10, 0x0a, 0x03, 0x66, 0x70, 0x73, 0x18, 0x08,
	0x20, 0x01, 0x28, 0x02, 0x52, 0x03, 0x66, 0x70, 0x73, 0x1a, 0x3e, 0x0a, 0x10, 0x4c, 0x61, 0x62,
	0x65, 0x6c, 0x46, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x45, 0x6e, 0x74, 0x72, 0x79, 0x12, 0x10, 0x0a,
	0x03, 0x6b, 0x65, 0x79, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x03, 0x6b, 0x65, 0x79, 0x12,
	0x14, 0x0a, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05,
	0x76, 0x61, 0x6c, 0x75, 0x65, 0x3a, 0x02, 0x38, 0x01, 0x22, 0x5e, 0x0a, 0x1a, 0x53, 0x75, 0x70,
	0x70, 0x6f, 0x72, 0x74, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x54, 0x79, 0x70, 0x65, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x74, 0x79, 0x70, 0x65, 0x73,
	0x18, 0x01, 0x20, 0x03, 0x28, 0x09, 0x52, 0x05, 0x74, 0x79, 0x70, 0x65, 0x73, 0x12, 0x2a, 0x0a,
	0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x12, 0x2e,
	0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x72, 0x70, 0x63, 0x2e, 0x53, 0x74, 0x61, 0x74, 0x75,
	0x73, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x22, 0x83, 0x02, 0x0a, 0x14, 0x43, 0x72,
	0x65, 0x61, 0x74, 0x65, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x12, 0x12, 0x0a, 0x04, 0x74, 0x79, 0x70, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x04, 0x74, 0x79, 0x70, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x02,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x63, 0x0a, 0x0c, 0x6c, 0x61,
	0x62, 0x65, 0x6c, 0x5f, 0x66, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x18, 0x03, 0x20, 0x03, 0x28, 0x0b,
	0x32, 0x40, 0x2e, 0x74, 0x6f, 0x75, 0x63, 0x68, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x70, 0x62,
	0x2e, 0x68, 0x61, 0x77, 0x6b, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x76, 0x31, 0x2e, 0x43, 0x72, 0x65,
	0x61, 0x74, 0x65, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x2e, 0x4c, 0x61, 0x62, 0x65, 0x6c, 0x46, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x45, 0x6e, 0x74,
	0x72, 0x79, 0x52, 0x0b, 0x6c, 0x61, 0x62, 0x65, 0x6c, 0x46, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x12,
	0x1e, 0x0a, 0x0a, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x64, 0x65, 0x6e, 0x63, 0x65, 0x18, 0x04, 0x20,
	0x01, 0x28, 0x02, 0x52, 0x0a, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x64, 0x65, 0x6e, 0x63, 0x65, 0x1a,
	0x3e, 0x0a, 0x10, 0x4c, 0x61, 0x62, 0x65, 0x6c, 0x46, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x45, 0x6e,
	0x74, 0x72, 0x79, 0x12, 0x10, 0x0a, 0x03, 0x6b, 0x65, 0x79, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0d,
	0x52, 0x03, 0x6b, 0x65, 0x79, 0x12, 0x14, 0x0a, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x02,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x3a, 0x02, 0x38, 0x01, 0x22,
	0x81, 0x01, 0x0a, 0x15, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65,
	0x6c, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x3c, 0x0a, 0x07, 0x63, 0x68, 0x61,
	0x6e, 0x6e, 0x65, 0x6c, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x22, 0x2e, 0x74, 0x6f, 0x75,
	0x63, 0x68, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x70, 0x62, 0x2e, 0x68, 0x61, 0x77, 0x6b, 0x2e,
	0x61, 0x70, 0x69, 0x2e, 0x76, 0x31, 0x2e, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x52, 0x07,
	0x63, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x12, 0x2a, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75,
	0x73, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x12, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65,
	0x2e, 0x72, 0x70, 0x63, 0x2e, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x52, 0x06, 0x73, 0x74, 0x61,
	0x74, 0x75, 0x73, 0x22, 0x81, 0x01, 0x0a, 0x13, 0x4c, 0x69, 0x73, 0x74, 0x43, 0x68, 0x61, 0x6e,
	0x6e, 0x65, 0x6c, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x3e, 0x0a, 0x08, 0x63,
	0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x22, 0x2e,
	0x74, 0x6f, 0x75, 0x63, 0x68, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x70, 0x62, 0x2e, 0x68, 0x61,
	0x77, 0x6b, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x76, 0x31, 0x2e, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65,
	0x6c, 0x52, 0x08, 0x63, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x73, 0x12, 0x2a, 0x0a, 0x06, 0x73,
	0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x12, 0x2e, 0x67, 0x6f,
	0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x72, 0x70, 0x63, 0x2e, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x52,
	0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x22, 0xef, 0x01, 0x0a, 0x14, 0x55, 0x70, 0x64, 0x61,
	0x74, 0x65, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04,
	0x6e, 0x61, 0x6d, 0x65, 0x12, 0x63, 0x0a, 0x0c, 0x6c, 0x61, 0x62, 0x65, 0x6c, 0x5f, 0x66, 0x69,
	0x6c, 0x74, 0x65, 0x72, 0x18, 0x02, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x40, 0x2e, 0x74, 0x6f, 0x75,
	0x63, 0x68, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x70, 0x62, 0x2e, 0x68, 0x61, 0x77, 0x6b, 0x2e,
	0x61, 0x70, 0x69, 0x2e, 0x76, 0x31, 0x2e, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x43, 0x68, 0x61,
	0x6e, 0x6e, 0x65, 0x6c, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x2e, 0x4c, 0x61, 0x62, 0x65,
	0x6c, 0x46, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x45, 0x6e, 0x74, 0x72, 0x79, 0x52, 0x0b, 0x6c, 0x61,
	0x62, 0x65, 0x6c, 0x46, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x12, 0x1e, 0x0a, 0x0a, 0x63, 0x6f, 0x6e,
	0x66, 0x69, 0x64, 0x65, 0x6e, 0x63, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x02, 0x52, 0x0a, 0x63,
	0x6f, 0x6e, 0x66, 0x69, 0x64, 0x65, 0x6e, 0x63, 0x65, 0x1a, 0x3e, 0x0a, 0x10, 0x4c, 0x61, 0x62,
	0x65, 0x6c, 0x46, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x45, 0x6e, 0x74, 0x72, 0x79, 0x12, 0x10, 0x0a,
	0x03, 0x6b, 0x65, 0x79, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x03, 0x6b, 0x65, 0x79, 0x12,
	0x14, 0x0a, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05,
	0x76, 0x61, 0x6c, 0x75, 0x65, 0x3a, 0x02, 0x38, 0x01, 0x22, 0x81, 0x01, 0x0a, 0x15, 0x55, 0x70,
	0x64, 0x61, 0x74, 0x65, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x52, 0x65, 0x73, 0x70, 0x6f,
	0x6e, 0x73, 0x65, 0x12, 0x3c, 0x0a, 0x07, 0x63, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x0b, 0x32, 0x22, 0x2e, 0x74, 0x6f, 0x75, 0x63, 0x68, 0x63, 0x6c, 0x6f, 0x75,
	0x64, 0x2e, 0x70, 0x62, 0x2e, 0x68, 0x61, 0x77, 0x6b, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x76, 0x31,
	0x2e, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x52, 0x07, 0x63, 0x68, 0x61, 0x6e, 0x6e, 0x65,
	0x6c, 0x12, 0x2a, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x0b, 0x32, 0x12, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x72, 0x70, 0x63, 0x2e, 0x53,
	0x74, 0x61, 0x74, 0x75, 0x73, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x22, 0x81, 0x01,
	0x0a, 0x15, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x3c, 0x0a, 0x07, 0x63, 0x68, 0x61, 0x6e, 0x6e,
	0x65, 0x6c, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x22, 0x2e, 0x74, 0x6f, 0x75, 0x63, 0x68,
	0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x70, 0x62, 0x2e, 0x68, 0x61, 0x77, 0x6b, 0x2e, 0x61, 0x70,
	0x69, 0x2e, 0x76, 0x31, 0x2e, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x52, 0x07, 0x63, 0x68,
	0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x12, 0x2a, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x12, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x72,
	0x70, 0x63, 0x2e, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75,
	0x73, 0x22, 0x7f, 0x0a, 0x13, 0x4d, 0x6f, 0x75, 0x6e, 0x74, 0x46, 0x65, 0x65, 0x64, 0x65, 0x72,
	0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x3c, 0x0a, 0x07, 0x63, 0x68, 0x61, 0x6e,
	0x6e, 0x65, 0x6c, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x22, 0x2e, 0x74, 0x6f, 0x75, 0x63,
	0x68, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x70, 0x62, 0x2e, 0x68, 0x61, 0x77, 0x6b, 0x2e, 0x61,
	0x70, 0x69, 0x2e, 0x76, 0x31, 0x2e, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x52, 0x07, 0x63,
	0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x12, 0x2a, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x12, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e,
	0x72, 0x70, 0x63, 0x2e, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74,
	0x75, 0x73, 0x22, 0x7f, 0x0a, 0x13, 0x4d, 0x6f, 0x75, 0x6e, 0x74, 0x45, 0x6e, 0x67, 0x69, 0x6e,
	0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x3c, 0x0a, 0x07, 0x63, 0x68, 0x61,
	0x6e, 0x6e, 0x65, 0x6c, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x22, 0x2e, 0x74, 0x6f, 0x75,
	0x63, 0x68, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x70, 0x62, 0x2e, 0x68, 0x61, 0x77, 0x6b, 0x2e,
	0x61, 0x70, 0x69, 0x2e, 0x76, 0x31, 0x2e, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x52, 0x07,
	0x63, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x12, 0x2a, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75,
	0x73, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x12, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65,
	0x2e, 0x72, 0x70, 0x63, 0x2e, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x52, 0x06, 0x73, 0x74, 0x61,
	0x74, 0x75, 0x73, 0x42, 0x29, 0x5a, 0x27, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f,
	0x6d, 0x2f, 0x74, 0x6f, 0x75, 0x63, 0x68, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2f, 0x70, 0x62, 0x2f,
	0x68, 0x61, 0x77, 0x6b, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x76, 0x31, 0x3b, 0x76, 0x31, 0x62, 0x06,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_v1_channel_proto_rawDescOnce sync.Once
	file_v1_channel_proto_rawDescData = file_v1_channel_proto_rawDesc
)

func file_v1_channel_proto_rawDescGZIP() []byte {
	file_v1_channel_proto_rawDescOnce.Do(func() {
		file_v1_channel_proto_rawDescData = protoimpl.X.CompressGZIP(file_v1_channel_proto_rawDescData)
	})
	return file_v1_channel_proto_rawDescData
}

var file_v1_channel_proto_msgTypes = make([]protoimpl.MessageInfo, 13)
var file_v1_channel_proto_goTypes = []interface{}{
	(*Channel)(nil),                    // 0: touchcloud.pb.hawk.api.v1.Channel
	(*SupportChannelTypeResponse)(nil), // 1: touchcloud.pb.hawk.api.v1.SupportChannelTypeResponse
	(*CreateChannelRequest)(nil),       // 2: touchcloud.pb.hawk.api.v1.CreateChannelRequest
	(*CreateChannelResponse)(nil),      // 3: touchcloud.pb.hawk.api.v1.CreateChannelResponse
	(*ListChannelResponse)(nil),        // 4: touchcloud.pb.hawk.api.v1.ListChannelResponse
	(*UpdateChannelRequest)(nil),       // 5: touchcloud.pb.hawk.api.v1.UpdateChannelRequest
	(*UpdateChannelResponse)(nil),      // 6: touchcloud.pb.hawk.api.v1.UpdateChannelResponse
	(*DeleteChannelResponse)(nil),      // 7: touchcloud.pb.hawk.api.v1.DeleteChannelResponse
	(*MountFeederResponse)(nil),        // 8: touchcloud.pb.hawk.api.v1.MountFeederResponse
	(*MountEngineResponse)(nil),        // 9: touchcloud.pb.hawk.api.v1.MountEngineResponse
	nil,                                // 10: touchcloud.pb.hawk.api.v1.Channel.LabelFilterEntry
	nil,                                // 11: touchcloud.pb.hawk.api.v1.CreateChannelRequest.LabelFilterEntry
	nil,                                // 12: touchcloud.pb.hawk.api.v1.UpdateChannelRequest.LabelFilterEntry
	(*status.Status)(nil),              // 13: google.rpc.Status
}
var file_v1_channel_proto_depIdxs = []int32{
	10, // 0: touchcloud.pb.hawk.api.v1.Channel.label_filter:type_name -> touchcloud.pb.hawk.api.v1.Channel.LabelFilterEntry
	13, // 1: touchcloud.pb.hawk.api.v1.SupportChannelTypeResponse.status:type_name -> google.rpc.Status
	11, // 2: touchcloud.pb.hawk.api.v1.CreateChannelRequest.label_filter:type_name -> touchcloud.pb.hawk.api.v1.CreateChannelRequest.LabelFilterEntry
	0,  // 3: touchcloud.pb.hawk.api.v1.CreateChannelResponse.channel:type_name -> touchcloud.pb.hawk.api.v1.Channel
	13, // 4: touchcloud.pb.hawk.api.v1.CreateChannelResponse.status:type_name -> google.rpc.Status
	0,  // 5: touchcloud.pb.hawk.api.v1.ListChannelResponse.channels:type_name -> touchcloud.pb.hawk.api.v1.Channel
	13, // 6: touchcloud.pb.hawk.api.v1.ListChannelResponse.status:type_name -> google.rpc.Status
	12, // 7: touchcloud.pb.hawk.api.v1.UpdateChannelRequest.label_filter:type_name -> touchcloud.pb.hawk.api.v1.UpdateChannelRequest.LabelFilterEntry
	0,  // 8: touchcloud.pb.hawk.api.v1.UpdateChannelResponse.channel:type_name -> touchcloud.pb.hawk.api.v1.Channel
	13, // 9: touchcloud.pb.hawk.api.v1.UpdateChannelResponse.status:type_name -> google.rpc.Status
	0,  // 10: touchcloud.pb.hawk.api.v1.DeleteChannelResponse.channel:type_name -> touchcloud.pb.hawk.api.v1.Channel
	13, // 11: touchcloud.pb.hawk.api.v1.DeleteChannelResponse.status:type_name -> google.rpc.Status
	0,  // 12: touchcloud.pb.hawk.api.v1.MountFeederResponse.channel:type_name -> touchcloud.pb.hawk.api.v1.Channel
	13, // 13: touchcloud.pb.hawk.api.v1.MountFeederResponse.status:type_name -> google.rpc.Status
	0,  // 14: touchcloud.pb.hawk.api.v1.MountEngineResponse.channel:type_name -> touchcloud.pb.hawk.api.v1.Channel
	13, // 15: touchcloud.pb.hawk.api.v1.MountEngineResponse.status:type_name -> google.rpc.Status
	16, // [16:16] is the sub-list for method output_type
	16, // [16:16] is the sub-list for method input_type
	16, // [16:16] is the sub-list for extension type_name
	16, // [16:16] is the sub-list for extension extendee
	0,  // [0:16] is the sub-list for field type_name
}

func init() { file_v1_channel_proto_init() }
func file_v1_channel_proto_init() {
	if File_v1_channel_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_v1_channel_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Channel); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_channel_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SupportChannelTypeResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_channel_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateChannelRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_channel_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateChannelResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_channel_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ListChannelResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_channel_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateChannelRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_channel_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateChannelResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_channel_proto_msgTypes[7].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DeleteChannelResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_channel_proto_msgTypes[8].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*MountFeederResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_channel_proto_msgTypes[9].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*MountEngineResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_v1_channel_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   13,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_v1_channel_proto_goTypes,
		DependencyIndexes: file_v1_channel_proto_depIdxs,
		MessageInfos:      file_v1_channel_proto_msgTypes,
	}.Build()
	File_v1_channel_proto = out.File
	file_v1_channel_proto_rawDesc = nil
	file_v1_channel_proto_goTypes = nil
	file_v1_channel_proto_depIdxs = nil
}
