// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.25.0
// 	protoc        v3.13.0
// source: v1/feeder.proto

package v1

import (
	proto "github.com/golang/protobuf/proto"
	status "google.golang.org/genproto/googleapis/rpc/status"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type Feeder struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Uuid     string  `protobuf:"bytes,1,opt,name=uuid,proto3" json:"uuid,omitempty"`
	Type     string  `protobuf:"bytes,2,opt,name=type,proto3" json:"type,omitempty"`
	Name     string  `protobuf:"bytes,3,opt,name=name,proto3" json:"name,omitempty"`
	Online   bool    `protobuf:"varint,4,opt,name=online,proto3" json:"online,omitempty"`
	Fps      float32 `protobuf:"fixed32,5,opt,name=fps,proto3" json:"fps,omitempty"`
	Source   string  `protobuf:"bytes,6,opt,name=source,proto3" json:"source,omitempty"`
	Username string  `protobuf:"bytes,7,opt,name=username,proto3" json:"username,omitempty"`
	Password string  `protobuf:"bytes,8,opt,name=password,proto3" json:"password,omitempty"`
}

func (x *Feeder) Reset() {
	*x = Feeder{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_feeder_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Feeder) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Feeder) ProtoMessage() {}

func (x *Feeder) ProtoReflect() protoreflect.Message {
	mi := &file_v1_feeder_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Feeder.ProtoReflect.Descriptor instead.
func (*Feeder) Descriptor() ([]byte, []int) {
	return file_v1_feeder_proto_rawDescGZIP(), []int{0}
}

func (x *Feeder) GetUuid() string {
	if x != nil {
		return x.Uuid
	}
	return ""
}

func (x *Feeder) GetType() string {
	if x != nil {
		return x.Type
	}
	return ""
}

func (x *Feeder) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *Feeder) GetOnline() bool {
	if x != nil {
		return x.Online
	}
	return false
}

func (x *Feeder) GetFps() float32 {
	if x != nil {
		return x.Fps
	}
	return 0
}

func (x *Feeder) GetSource() string {
	if x != nil {
		return x.Source
	}
	return ""
}

func (x *Feeder) GetUsername() string {
	if x != nil {
		return x.Username
	}
	return ""
}

func (x *Feeder) GetPassword() string {
	if x != nil {
		return x.Password
	}
	return ""
}

// Method: GET
// URL: /v1/support/type/feeder
type SupportFeederTypeResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Types  []string       `protobuf:"bytes,1,rep,name=types,proto3" json:"types,omitempty"`
	Status *status.Status `protobuf:"bytes,2,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *SupportFeederTypeResponse) Reset() {
	*x = SupportFeederTypeResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_feeder_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SupportFeederTypeResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SupportFeederTypeResponse) ProtoMessage() {}

func (x *SupportFeederTypeResponse) ProtoReflect() protoreflect.Message {
	mi := &file_v1_feeder_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SupportFeederTypeResponse.ProtoReflect.Descriptor instead.
func (*SupportFeederTypeResponse) Descriptor() ([]byte, []int) {
	return file_v1_feeder_proto_rawDescGZIP(), []int{1}
}

func (x *SupportFeederTypeResponse) GetTypes() []string {
	if x != nil {
		return x.Types
	}
	return nil
}

func (x *SupportFeederTypeResponse) GetStatus() *status.Status {
	if x != nil {
		return x.Status
	}
	return nil
}

// Method:  POST
// URL:     /v1/feeders
type CreateFeederRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Type string `protobuf:"bytes,1,opt,name=type,proto3" json:"type,omitempty"`
	// for vms at stream type
	VmsUuid string `protobuf:"bytes,2,opt,name=vms_uuid,json=vmsUuid,proto3" json:"vms_uuid,omitempty"`
	CamUuid string `protobuf:"bytes,3,opt,name=cam_uuid,json=camUuid,proto3" json:"cam_uuid,omitempty"`
	// for camera at stream type
	Name     string `protobuf:"bytes,4,opt,name=name,proto3" json:"name,omitempty"`
	Source   string `protobuf:"bytes,5,opt,name=source,proto3" json:"source,omitempty"`
	Username string `protobuf:"bytes,6,opt,name=username,proto3" json:"username,omitempty"`
	Password string `protobuf:"bytes,7,opt,name=password,proto3" json:"password,omitempty"`
}

func (x *CreateFeederRequest) Reset() {
	*x = CreateFeederRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_feeder_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateFeederRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateFeederRequest) ProtoMessage() {}

func (x *CreateFeederRequest) ProtoReflect() protoreflect.Message {
	mi := &file_v1_feeder_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateFeederRequest.ProtoReflect.Descriptor instead.
func (*CreateFeederRequest) Descriptor() ([]byte, []int) {
	return file_v1_feeder_proto_rawDescGZIP(), []int{2}
}

func (x *CreateFeederRequest) GetType() string {
	if x != nil {
		return x.Type
	}
	return ""
}

func (x *CreateFeederRequest) GetVmsUuid() string {
	if x != nil {
		return x.VmsUuid
	}
	return ""
}

func (x *CreateFeederRequest) GetCamUuid() string {
	if x != nil {
		return x.CamUuid
	}
	return ""
}

func (x *CreateFeederRequest) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *CreateFeederRequest) GetSource() string {
	if x != nil {
		return x.Source
	}
	return ""
}

func (x *CreateFeederRequest) GetUsername() string {
	if x != nil {
		return x.Username
	}
	return ""
}

func (x *CreateFeederRequest) GetPassword() string {
	if x != nil {
		return x.Password
	}
	return ""
}

type CreateFeederResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Feeder *Feeder        `protobuf:"bytes,1,opt,name=feeder,proto3" json:"feeder,omitempty"`
	Status *status.Status `protobuf:"bytes,2,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *CreateFeederResponse) Reset() {
	*x = CreateFeederResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_feeder_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateFeederResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateFeederResponse) ProtoMessage() {}

func (x *CreateFeederResponse) ProtoReflect() protoreflect.Message {
	mi := &file_v1_feeder_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateFeederResponse.ProtoReflect.Descriptor instead.
func (*CreateFeederResponse) Descriptor() ([]byte, []int) {
	return file_v1_feeder_proto_rawDescGZIP(), []int{3}
}

func (x *CreateFeederResponse) GetFeeder() *Feeder {
	if x != nil {
		return x.Feeder
	}
	return nil
}

func (x *CreateFeederResponse) GetStatus() *status.Status {
	if x != nil {
		return x.Status
	}
	return nil
}

// Method:  PUT
// URL:     /v1/feeders/:uuid
type UpdateFeederRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Name     string `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	Username string `protobuf:"bytes,2,opt,name=username,proto3" json:"username,omitempty"`
	Password string `protobuf:"bytes,3,opt,name=password,proto3" json:"password,omitempty"`
}

func (x *UpdateFeederRequest) Reset() {
	*x = UpdateFeederRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_feeder_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateFeederRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateFeederRequest) ProtoMessage() {}

func (x *UpdateFeederRequest) ProtoReflect() protoreflect.Message {
	mi := &file_v1_feeder_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateFeederRequest.ProtoReflect.Descriptor instead.
func (*UpdateFeederRequest) Descriptor() ([]byte, []int) {
	return file_v1_feeder_proto_rawDescGZIP(), []int{4}
}

func (x *UpdateFeederRequest) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *UpdateFeederRequest) GetUsername() string {
	if x != nil {
		return x.Username
	}
	return ""
}

func (x *UpdateFeederRequest) GetPassword() string {
	if x != nil {
		return x.Password
	}
	return ""
}

type UpdateFeederResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Feeder *Feeder        `protobuf:"bytes,1,opt,name=feeder,proto3" json:"feeder,omitempty"`
	Status *status.Status `protobuf:"bytes,2,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *UpdateFeederResponse) Reset() {
	*x = UpdateFeederResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_feeder_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateFeederResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateFeederResponse) ProtoMessage() {}

func (x *UpdateFeederResponse) ProtoReflect() protoreflect.Message {
	mi := &file_v1_feeder_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateFeederResponse.ProtoReflect.Descriptor instead.
func (*UpdateFeederResponse) Descriptor() ([]byte, []int) {
	return file_v1_feeder_proto_rawDescGZIP(), []int{5}
}

func (x *UpdateFeederResponse) GetFeeder() *Feeder {
	if x != nil {
		return x.Feeder
	}
	return nil
}

func (x *UpdateFeederResponse) GetStatus() *status.Status {
	if x != nil {
		return x.Status
	}
	return nil
}

// Method:  DELETE
// URL:     /v1/feeders/:uuid
type DeleteFeederResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Feeder *Feeder        `protobuf:"bytes,1,opt,name=feeder,proto3" json:"feeder,omitempty"`
	Status *status.Status `protobuf:"bytes,2,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *DeleteFeederResponse) Reset() {
	*x = DeleteFeederResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_feeder_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DeleteFeederResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DeleteFeederResponse) ProtoMessage() {}

func (x *DeleteFeederResponse) ProtoReflect() protoreflect.Message {
	mi := &file_v1_feeder_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DeleteFeederResponse.ProtoReflect.Descriptor instead.
func (*DeleteFeederResponse) Descriptor() ([]byte, []int) {
	return file_v1_feeder_proto_rawDescGZIP(), []int{6}
}

func (x *DeleteFeederResponse) GetFeeder() *Feeder {
	if x != nil {
		return x.Feeder
	}
	return nil
}

func (x *DeleteFeederResponse) GetStatus() *status.Status {
	if x != nil {
		return x.Status
	}
	return nil
}

// Method:  GET
// URL:     /v1/feeders
type ListFeederResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Feeders []*Feeder      `protobuf:"bytes,1,rep,name=feeders,proto3" json:"feeders,omitempty"`
	Status  *status.Status `protobuf:"bytes,2,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *ListFeederResponse) Reset() {
	*x = ListFeederResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_feeder_proto_msgTypes[7]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ListFeederResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ListFeederResponse) ProtoMessage() {}

func (x *ListFeederResponse) ProtoReflect() protoreflect.Message {
	mi := &file_v1_feeder_proto_msgTypes[7]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ListFeederResponse.ProtoReflect.Descriptor instead.
func (*ListFeederResponse) Descriptor() ([]byte, []int) {
	return file_v1_feeder_proto_rawDescGZIP(), []int{7}
}

func (x *ListFeederResponse) GetFeeders() []*Feeder {
	if x != nil {
		return x.Feeders
	}
	return nil
}

func (x *ListFeederResponse) GetStatus() *status.Status {
	if x != nil {
		return x.Status
	}
	return nil
}

// Method:  GET
// URL:     /v1/feeders/:uuid/snapshot
type SnapshotFeederResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Data   string         `protobuf:"bytes,1,opt,name=data,proto3" json:"data,omitempty"`
	Size   *Point         `protobuf:"bytes,2,opt,name=size,proto3" json:"size,omitempty"`
	Status *status.Status `protobuf:"bytes,3,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *SnapshotFeederResponse) Reset() {
	*x = SnapshotFeederResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_feeder_proto_msgTypes[8]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SnapshotFeederResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SnapshotFeederResponse) ProtoMessage() {}

func (x *SnapshotFeederResponse) ProtoReflect() protoreflect.Message {
	mi := &file_v1_feeder_proto_msgTypes[8]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SnapshotFeederResponse.ProtoReflect.Descriptor instead.
func (*SnapshotFeederResponse) Descriptor() ([]byte, []int) {
	return file_v1_feeder_proto_rawDescGZIP(), []int{8}
}

func (x *SnapshotFeederResponse) GetData() string {
	if x != nil {
		return x.Data
	}
	return ""
}

func (x *SnapshotFeederResponse) GetSize() *Point {
	if x != nil {
		return x.Size
	}
	return nil
}

func (x *SnapshotFeederResponse) GetStatus() *status.Status {
	if x != nil {
		return x.Status
	}
	return nil
}

var File_v1_feeder_proto protoreflect.FileDescriptor

var file_v1_feeder_proto_rawDesc = []byte{
	0x0a, 0x0f, 0x76, 0x31, 0x2f, 0x66, 0x65, 0x65, 0x64, 0x65, 0x72, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x12, 0x19, 0x74, 0x6f, 0x75, 0x63, 0x68, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x70, 0x62,
	0x2e, 0x68, 0x61, 0x77, 0x6b, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x76, 0x31, 0x1a, 0x1e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x2f, 0x67, 0x72, 0x70, 0x63, 0x2f, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x2f,
	0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x11, 0x76, 0x31,
	0x2f, 0x67, 0x75, 0x61, 0x72, 0x64, 0x69, 0x61, 0x6e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22,
	0xbe, 0x01, 0x0a, 0x06, 0x46, 0x65, 0x65, 0x64, 0x65, 0x72, 0x12, 0x12, 0x0a, 0x04, 0x75, 0x75,
	0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x75, 0x75, 0x69, 0x64, 0x12, 0x12,
	0x0a, 0x04, 0x74, 0x79, 0x70, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x74, 0x79,
	0x70, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x16, 0x0a, 0x06, 0x6f, 0x6e, 0x6c, 0x69, 0x6e, 0x65,
	0x18, 0x04, 0x20, 0x01, 0x28, 0x08, 0x52, 0x06, 0x6f, 0x6e, 0x6c, 0x69, 0x6e, 0x65, 0x12, 0x10,
	0x0a, 0x03, 0x66, 0x70, 0x73, 0x18, 0x05, 0x20, 0x01, 0x28, 0x02, 0x52, 0x03, 0x66, 0x70, 0x73,
	0x12, 0x16, 0x0a, 0x06, 0x73, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x06, 0x73, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x12, 0x1a, 0x0a, 0x08, 0x75, 0x73, 0x65, 0x72,
	0x6e, 0x61, 0x6d, 0x65, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x75, 0x73, 0x65, 0x72,
	0x6e, 0x61, 0x6d, 0x65, 0x12, 0x1a, 0x0a, 0x08, 0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64,
	0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64,
	0x22, 0x5d, 0x0a, 0x19, 0x53, 0x75, 0x70, 0x70, 0x6f, 0x72, 0x74, 0x46, 0x65, 0x65, 0x64, 0x65,
	0x72, 0x54, 0x79, 0x70, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x14, 0x0a,
	0x05, 0x74, 0x79, 0x70, 0x65, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x09, 0x52, 0x05, 0x74, 0x79,
	0x70, 0x65, 0x73, 0x12, 0x2a, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x0b, 0x32, 0x12, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x72, 0x70, 0x63,
	0x2e, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x22,
	0xc3, 0x01, 0x0a, 0x13, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x46, 0x65, 0x65, 0x64, 0x65, 0x72,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x12, 0x0a, 0x04, 0x74, 0x79, 0x70, 0x65, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x74, 0x79, 0x70, 0x65, 0x12, 0x19, 0x0a, 0x08, 0x76,
	0x6d, 0x73, 0x5f, 0x75, 0x75, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x76,
	0x6d, 0x73, 0x55, 0x75, 0x69, 0x64, 0x12, 0x19, 0x0a, 0x08, 0x63, 0x61, 0x6d, 0x5f, 0x75, 0x75,
	0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x63, 0x61, 0x6d, 0x55, 0x75, 0x69,
	0x64, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x18,
	0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x12, 0x1a, 0x0a,
	0x08, 0x75, 0x73, 0x65, 0x72, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x08, 0x75, 0x73, 0x65, 0x72, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x1a, 0x0a, 0x08, 0x70, 0x61, 0x73,
	0x73, 0x77, 0x6f, 0x72, 0x64, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x70, 0x61, 0x73,
	0x73, 0x77, 0x6f, 0x72, 0x64, 0x22, 0x7d, 0x0a, 0x14, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x46,
	0x65, 0x65, 0x64, 0x65, 0x72, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x39, 0x0a,
	0x06, 0x66, 0x65, 0x65, 0x64, 0x65, 0x72, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x21, 0x2e,
	0x74, 0x6f, 0x75, 0x63, 0x68, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x70, 0x62, 0x2e, 0x68, 0x61,
	0x77, 0x6b, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x76, 0x31, 0x2e, 0x46, 0x65, 0x65, 0x64, 0x65, 0x72,
	0x52, 0x06, 0x66, 0x65, 0x65, 0x64, 0x65, 0x72, 0x12, 0x2a, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74,
	0x75, 0x73, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x12, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c,
	0x65, 0x2e, 0x72, 0x70, 0x63, 0x2e, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x52, 0x06, 0x73, 0x74,
	0x61, 0x74, 0x75, 0x73, 0x22, 0x61, 0x0a, 0x13, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x46, 0x65,
	0x65, 0x64, 0x65, 0x72, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x12, 0x0a, 0x04, 0x6e,
	0x61, 0x6d, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12,
	0x1a, 0x0a, 0x08, 0x75, 0x73, 0x65, 0x72, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x08, 0x75, 0x73, 0x65, 0x72, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x1a, 0x0a, 0x08, 0x70,
	0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x70,
	0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x22, 0x7d, 0x0a, 0x14, 0x55, 0x70, 0x64, 0x61, 0x74,
	0x65, 0x46, 0x65, 0x65, 0x64, 0x65, 0x72, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12,
	0x39, 0x0a, 0x06, 0x66, 0x65, 0x65, 0x64, 0x65, 0x72, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32,
	0x21, 0x2e, 0x74, 0x6f, 0x75, 0x63, 0x68, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x70, 0x62, 0x2e,
	0x68, 0x61, 0x77, 0x6b, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x76, 0x31, 0x2e, 0x46, 0x65, 0x65, 0x64,
	0x65, 0x72, 0x52, 0x06, 0x66, 0x65, 0x65, 0x64, 0x65, 0x72, 0x12, 0x2a, 0x0a, 0x06, 0x73, 0x74,
	0x61, 0x74, 0x75, 0x73, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x12, 0x2e, 0x67, 0x6f, 0x6f,
	0x67, 0x6c, 0x65, 0x2e, 0x72, 0x70, 0x63, 0x2e, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x52, 0x06,
	0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x22, 0x7d, 0x0a, 0x14, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65,
	0x46, 0x65, 0x65, 0x64, 0x65, 0x72, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x39,
	0x0a, 0x06, 0x66, 0x65, 0x65, 0x64, 0x65, 0x72, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x21,
	0x2e, 0x74, 0x6f, 0x75, 0x63, 0x68, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x70, 0x62, 0x2e, 0x68,
	0x61, 0x77, 0x6b, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x76, 0x31, 0x2e, 0x46, 0x65, 0x65, 0x64, 0x65,
	0x72, 0x52, 0x06, 0x66, 0x65, 0x65, 0x64, 0x65, 0x72, 0x12, 0x2a, 0x0a, 0x06, 0x73, 0x74, 0x61,
	0x74, 0x75, 0x73, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x12, 0x2e, 0x67, 0x6f, 0x6f, 0x67,
	0x6c, 0x65, 0x2e, 0x72, 0x70, 0x63, 0x2e, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x52, 0x06, 0x73,
	0x74, 0x61, 0x74, 0x75, 0x73, 0x22, 0x7d, 0x0a, 0x12, 0x4c, 0x69, 0x73, 0x74, 0x46, 0x65, 0x65,
	0x64, 0x65, 0x72, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x3b, 0x0a, 0x07, 0x66,
	0x65, 0x65, 0x64, 0x65, 0x72, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x21, 0x2e, 0x74,
	0x6f, 0x75, 0x63, 0x68, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x70, 0x62, 0x2e, 0x68, 0x61, 0x77,
	0x6b, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x76, 0x31, 0x2e, 0x46, 0x65, 0x65, 0x64, 0x65, 0x72, 0x52,
	0x07, 0x66, 0x65, 0x65, 0x64, 0x65, 0x72, 0x73, 0x12, 0x2a, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74,
	0x75, 0x73, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x12, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c,
	0x65, 0x2e, 0x72, 0x70, 0x63, 0x2e, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x52, 0x06, 0x73, 0x74,
	0x61, 0x74, 0x75, 0x73, 0x22, 0x8e, 0x01, 0x0a, 0x16, 0x53, 0x6e, 0x61, 0x70, 0x73, 0x68, 0x6f,
	0x74, 0x46, 0x65, 0x65, 0x64, 0x65, 0x72, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12,
	0x12, 0x0a, 0x04, 0x64, 0x61, 0x74, 0x61, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x64,
	0x61, 0x74, 0x61, 0x12, 0x34, 0x0a, 0x04, 0x73, 0x69, 0x7a, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x0b, 0x32, 0x20, 0x2e, 0x74, 0x6f, 0x75, 0x63, 0x68, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x70,
	0x62, 0x2e, 0x68, 0x61, 0x77, 0x6b, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x76, 0x31, 0x2e, 0x50, 0x6f,
	0x69, 0x6e, 0x74, 0x52, 0x04, 0x73, 0x69, 0x7a, 0x65, 0x12, 0x2a, 0x0a, 0x06, 0x73, 0x74, 0x61,
	0x74, 0x75, 0x73, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x12, 0x2e, 0x67, 0x6f, 0x6f, 0x67,
	0x6c, 0x65, 0x2e, 0x72, 0x70, 0x63, 0x2e, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x52, 0x06, 0x73,
	0x74, 0x61, 0x74, 0x75, 0x73, 0x42, 0x29, 0x5a, 0x27, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e,
	0x63, 0x6f, 0x6d, 0x2f, 0x74, 0x6f, 0x75, 0x63, 0x68, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2f, 0x70,
	0x62, 0x2f, 0x68, 0x61, 0x77, 0x6b, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x76, 0x31, 0x3b, 0x76, 0x31,
	0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_v1_feeder_proto_rawDescOnce sync.Once
	file_v1_feeder_proto_rawDescData = file_v1_feeder_proto_rawDesc
)

func file_v1_feeder_proto_rawDescGZIP() []byte {
	file_v1_feeder_proto_rawDescOnce.Do(func() {
		file_v1_feeder_proto_rawDescData = protoimpl.X.CompressGZIP(file_v1_feeder_proto_rawDescData)
	})
	return file_v1_feeder_proto_rawDescData
}

var file_v1_feeder_proto_msgTypes = make([]protoimpl.MessageInfo, 9)
var file_v1_feeder_proto_goTypes = []interface{}{
	(*Feeder)(nil),                    // 0: touchcloud.pb.hawk.api.v1.Feeder
	(*SupportFeederTypeResponse)(nil), // 1: touchcloud.pb.hawk.api.v1.SupportFeederTypeResponse
	(*CreateFeederRequest)(nil),       // 2: touchcloud.pb.hawk.api.v1.CreateFeederRequest
	(*CreateFeederResponse)(nil),      // 3: touchcloud.pb.hawk.api.v1.CreateFeederResponse
	(*UpdateFeederRequest)(nil),       // 4: touchcloud.pb.hawk.api.v1.UpdateFeederRequest
	(*UpdateFeederResponse)(nil),      // 5: touchcloud.pb.hawk.api.v1.UpdateFeederResponse
	(*DeleteFeederResponse)(nil),      // 6: touchcloud.pb.hawk.api.v1.DeleteFeederResponse
	(*ListFeederResponse)(nil),        // 7: touchcloud.pb.hawk.api.v1.ListFeederResponse
	(*SnapshotFeederResponse)(nil),    // 8: touchcloud.pb.hawk.api.v1.SnapshotFeederResponse
	(*status.Status)(nil),             // 9: google.rpc.Status
	(*Point)(nil),                     // 10: touchcloud.pb.hawk.api.v1.Point
}
var file_v1_feeder_proto_depIdxs = []int32{
	9,  // 0: touchcloud.pb.hawk.api.v1.SupportFeederTypeResponse.status:type_name -> google.rpc.Status
	0,  // 1: touchcloud.pb.hawk.api.v1.CreateFeederResponse.feeder:type_name -> touchcloud.pb.hawk.api.v1.Feeder
	9,  // 2: touchcloud.pb.hawk.api.v1.CreateFeederResponse.status:type_name -> google.rpc.Status
	0,  // 3: touchcloud.pb.hawk.api.v1.UpdateFeederResponse.feeder:type_name -> touchcloud.pb.hawk.api.v1.Feeder
	9,  // 4: touchcloud.pb.hawk.api.v1.UpdateFeederResponse.status:type_name -> google.rpc.Status
	0,  // 5: touchcloud.pb.hawk.api.v1.DeleteFeederResponse.feeder:type_name -> touchcloud.pb.hawk.api.v1.Feeder
	9,  // 6: touchcloud.pb.hawk.api.v1.DeleteFeederResponse.status:type_name -> google.rpc.Status
	0,  // 7: touchcloud.pb.hawk.api.v1.ListFeederResponse.feeders:type_name -> touchcloud.pb.hawk.api.v1.Feeder
	9,  // 8: touchcloud.pb.hawk.api.v1.ListFeederResponse.status:type_name -> google.rpc.Status
	10, // 9: touchcloud.pb.hawk.api.v1.SnapshotFeederResponse.size:type_name -> touchcloud.pb.hawk.api.v1.Point
	9,  // 10: touchcloud.pb.hawk.api.v1.SnapshotFeederResponse.status:type_name -> google.rpc.Status
	11, // [11:11] is the sub-list for method output_type
	11, // [11:11] is the sub-list for method input_type
	11, // [11:11] is the sub-list for extension type_name
	11, // [11:11] is the sub-list for extension extendee
	0,  // [0:11] is the sub-list for field type_name
}

func init() { file_v1_feeder_proto_init() }
func file_v1_feeder_proto_init() {
	if File_v1_feeder_proto != nil {
		return
	}
	file_v1_guardian_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_v1_feeder_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Feeder); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_feeder_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SupportFeederTypeResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_feeder_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateFeederRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_feeder_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateFeederResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_feeder_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateFeederRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_feeder_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateFeederResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_feeder_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DeleteFeederResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_feeder_proto_msgTypes[7].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ListFeederResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_feeder_proto_msgTypes[8].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SnapshotFeederResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_v1_feeder_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   9,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_v1_feeder_proto_goTypes,
		DependencyIndexes: file_v1_feeder_proto_depIdxs,
		MessageInfos:      file_v1_feeder_proto_msgTypes,
	}.Build()
	File_v1_feeder_proto = out.File
	file_v1_feeder_proto_rawDesc = nil
	file_v1_feeder_proto_goTypes = nil
	file_v1_feeder_proto_depIdxs = nil
}
